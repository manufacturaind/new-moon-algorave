# new-moon-algorave

Designs and assets for the New Moon Algorave marathon event organised at Tidal Club (18 August 2020)

![New Moon banner](https://gitlab.com/manufacturaind/new-moon-algorave/-/raw/master/png/newmoon-4sq-banner.png)

The root dir contains the editable files (SVG and XCF) and the `png/` dir contains ready-to-use bitmap exports.

## Contents

- the filenames identify which version of the new moon illustration is used: `1sq` (single illustration) and `4sq` (with 4 panels)
- colors are dark blue (`#243d5c`), a kind of khaki (`#b1a58d`) and almost-black (`#1a1a1a`)

### Bitmap version links

For quick access to the existing assets:

- [1sq poster](https://gitlab.com/manufacturaind/new-moon-algorave/-/raw/master/png/newmoon-1sq-poster.png)
- [1sq poster with artist list](https://gitlab.com/manufacturaind/new-moon-algorave/-/raw/master/png/newmoon-1sq-poster-artists.png)
- [4sq banner](https://gitlab.com/manufacturaind/new-moon-algorave/-/raw/master/png/newmoon-4sq-banner.png)
- [artist list](https://gitlab.com/manufacturaind/new-moon-algorave/-/raw/master/png/newmoon-artists.png)
- [1sq pixelart version](https://gitlab.com/manufacturaind/new-moon-algorave/-/raw/master/png/newmoon-1sq-pixel-20x.png) (see also the [1:1 original pixel version](https://gitlab.com/manufacturaind/new-moon-algorave/-/raw/master/png/newmoon-1sq-pixel.png))

### License

All these assets are made available under the [CC Attribution-Sharealike license](https://creativecommons.org/licenses/by-sa/4.0/) (BY-SA).
